import React from "react";
import "./header.css";
import { Link, withRouter } from "react-router-dom";
import Logo from "../../../assets/img/logo.png";
import { Layout, Menu, Icon } from "antd";

class Header extends React.Component {
    render() {
        const navLink = this.props.location.pathname.match(/\/([^/]+)\/?/);
        let navKey = "dashboard";

        if (navLink) {
            navKey = navLink[1];
        }

        return (
            <Layout.Header className="Header">
                <Link className="Logo" to="/">
                    <img src={Logo} alt="KEİS" />
                </Link>
                <Menu
                    theme="light"
                    mode="horizontal"
                    defaultSelectedKeys={[navKey]}
                    style={{ lineHeight: "62px", borderBottom: "none" }}
                >
                    <Menu.Item key="dashboard">
                        <Link to="/dashboard">
                            <Icon type="dashboard" /> Lövhə
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="volunteers">
                        <Link to="/volunteers">
                            <Icon type="team" /> Könüllü Bazası
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="assessment">
                        <Link to="/assessment">
                            <Icon type="star-o" /> Qiymətləndirmə
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="settings">
                        <Link to="/settings">
                            <Icon type="setting" /> Parametrlər
                        </Link>
                    </Menu.Item>
                </Menu>
            </Layout.Header>
        );
    }
}

export default withRouter(Header);
