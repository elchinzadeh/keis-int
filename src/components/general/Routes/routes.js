import React from 'react';
import { Switch, Route} from 'react-router-dom';
import { NotFound } from "../../../pages/components/Error/error";
import AsyncDashboard from "../../../pages/async/AsyncDashboard/asyncDashboard";
import AsyncSettings from "../../../pages/async/AsyncSettings/asyncSettings";
import AsyncVolunteers from "../../../pages/async/AsyncVolunteers/asyncVolunteers";

const Routes = () => {
    return(
        <Switch>
            {/*<Route path="/assessment" component={AsyncVolunteers} />*/}
            <Route path="/dashboard" component={AsyncDashboard} />
            {/*<Route path="/events" component={AsyncVolunteers} />*/}
            {/*<Route path="/interview" component={AsyncVolunteers} />*/}
            <Route path="/settings" component={AsyncSettings} />
            <Route path="/volunteers" component={AsyncVolunteers} />
            <Route component={NotFound} />
        </Switch>
    )
};

export default Routes