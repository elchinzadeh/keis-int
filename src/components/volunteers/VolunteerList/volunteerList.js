import React from 'react';
import { Card, List, Avatar } from 'antd'
import Axios from "axios/index";

export default class VolunteerList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            volunteers: []
        };
    }

    componentDidMount() {
        this.fetchVolunteers();
    }

    fetchVolunteers = () => {
        // if state is empty
        Axios("https://jsonplaceholder.typicode.com/users")
            .then(res => {
                console.log(res);
                this.setState({
                    volunteers: res.data
                });
            })
            .catch(err => {
                console.error(err);
            });
    };

    render() {
        return(
            <Card bordered={false} className="VolunteerList" style={{ display: this.props.visible ? 'block' : 'none'}}>
                <List
                    loading={this.state.loading}
                    itemLayout="horizontal"
                    dataSource={this.state.volunteers}
                    renderItem={item => (
                        <List.Item className="VolunteerListItem">
                            <List.Item.Meta
                                avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                                title={<a onClick={() => this.props.showProfile(item.id)} >{item.name}</a>}
                                description={item.company.catchPhrase}
                            />
                            <div>{item.company.name}</div>
                        </List.Item>
                    )}
                />
            </Card>
        )
    }
}