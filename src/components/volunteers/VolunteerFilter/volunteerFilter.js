import React from 'react';
import { Row, Card, Icon, Button, Input, Select, Collapse } from 'antd'

const Option = Select.Option;
const Panel = Collapse.Panel;

export default class VolunteerFilter extends React.Component {

    render() {

        return(
            <Card bordered={false} style={{display: this.props.visible ? 'block' : 'none'}}>
                <h3><Icon type="filter" /> Filter</h3>
                <Collapse bordered={false} defaultActiveKey={['1', '2']}>
                    <Panel header="Şəxsi məlumatlar" key="1">
                        <Row style={styles.row}>
                            <Input placeholder="Ad, Soyad, Ata adı"
                                   prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            />
                        </Row>
                        <Row style={styles.row}>
                            <Input placeholder="Ş.V. seriya"
                                   prefix={<Icon type="idcard" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            />
                        </Row>
                        <Row style={styles.row}>
                            <Input placeholder="FİN"
                                   prefix={<Icon type="idcard" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            />
                        </Row>
                    </Panel>
                    <Panel header="Könüllülük məlumatları" key="2">
                        <Row style={styles.row}>
                            <Select defaultValue="0"
                                    style={styles.fullWidth}
                            >
                                <Option value="0" disabled>Mərkəzi seçin</Option>
                                <Option value="1">1 saylı ASAN Xidmət Mərkəzi</Option>
                                <Option value="2">2 saylı ASAN Xidmət Mərkəzi</Option>
                                <Option value="3">3 saylı ASAN Xidmət Mərkəzi</Option>
                                <Option value="4">4 saylı ASAN Xidmət Mərkəzi</Option>
                            </Select>
                        </Row>
                        <Row style={styles.row}>
                            <Input placeholder="Könüllü nömrəsi"
                                   prefix={<Icon type="solution" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            />
                        </Row>
                        <Row style={styles.row}>
                            <Select showSearch
                                    optionFilterProp="name"
                                    defaultValue="0"
                                    style={styles.fullWidth}
                            >
                                <Option value="0" disabled>Qrupu seçin</Option>
                                <Option value="1" name="656a1">656a1</Option>
                                <Option value="2" name="656a2">656a2</Option>
                                <Option value="3" name="656a3">656a3</Option>
                            </Select>
                        </Row>
                    </Panel>
                </Collapse>
                <Row style={styles.row}>
                    <Button type="primary" style={styles.fullWidth}>Filterlə</Button>
                </Row>
            </Card>
        )
    }
}

const styles = {
    row: {
        marginBottom: 10,
    },
    fullWidth: {
        width: '100%'
    }
};