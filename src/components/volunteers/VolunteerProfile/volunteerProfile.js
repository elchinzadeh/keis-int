import React from 'react'
import { Card, Button, Tabs } from 'antd'
import ProfileApi from '../../../api/m1/volunteers/ProfileApi'
import PersonalInfo from './personalInfo'

const TabPane = Tabs.TabPane;

export default class VolunteerProfile extends React.Component {
    constructor(){
        super();

        this.state = {
            data: null
        }
    }

    componentDidMount() {
        this.getVolunteerInfo()
    }

    getVolnteerInfo = () => {
        ProfileApi.getByUserId(this.props.selectedEntity).then(response => {
            if (response.data.error === null){
                this.setState({
                    data: response.data.data
                })
            }
        })
    };

    render() {
        return(
            <React.Fragment>
                <Button type="primary"
                        ghost
                        size="large"
                        icon="left"
                        style={styles.backBtn}
                        onClick={() => this.props.closeProfile}
                >
                    Back to the Future
                </Button>
                <Card bordered={false}>
                    <Tabs
                        defaultActiveKey="2"
                        tabPosition="top"
                    >
                        <TabPane tab="Könüllülük fəaliyyəti" key="1">
                            Könüllülük fəaliyyəti
                        </TabPane>
                        <TabPane tab="Şəxsi məlumatlar" key="2">
                            <PersonalInfo/>
                        </TabPane>
                        <TabPane tab="Təhsil" key="3">
                            Təhsil məlumatları
                        </TabPane>
                        <TabPane tab="Təcrübə   " key="4">
                            Təcrübə məlumatları
                        </TabPane>
                        <TabPane tab="Ailə məlumatları" key="5">
                            Ailə məlumatları
                        </TabPane>
                        <TabPane tab="Bilik və bacarıqlar" key="6">
                            Bilik və bacarıqlar
                        </TabPane>
                    </Tabs>
                </Card>
            </React.Fragment>
        )
    }
}

const styles = {
    backBtn: {
        border: 0,
        padding: 0
    },
    breadcrumb: {
        padding: '12px 0'
    }
};