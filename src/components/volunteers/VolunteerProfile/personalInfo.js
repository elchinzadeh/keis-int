import React from 'react';
import { Col, Row, Avatar } from 'antd';

const PersonalInfo = () => {
    return(
        <Row style={{textAlign: 'center'}}>
            <Avatar size={128} src="https://t2.genius.com/unsafe/220x220/https%3A%2F%2Fimages.genius.com%2Fd755026b56e1a231bf2a8728a4a0a37b.939x939x1.jpg" />

            <h2>Elchin Zakizadeh</h2>
            <h3 style={styles.color.secondary}>1 saylı ASAN Xidmət</h3>

            <Row>
                <Col md={12}>
            <Row>
                <Col span={12} style={styles.heading}>Doğum tarixi:</Col>
                <Col span={12} style={styles.desc}><b>9 Mart 1999</b></Col>
            </Row>
            <Row>
                <Col span={12} style={styles.heading}>Doğum yeri:</Col>
                <Col span={12} style={styles.desc}><b>Shabran city, Azerbaijan</b></Col>
            </Row>
            <Row>
                <Col span={12} style={styles.heading}>Cinsi:</Col>
                <Col span={12} style={styles.desc}><b>Kişi</b></Col>
            </Row>
            <Row>
                <Col span={12} style={styles.heading}>Ailə vəziyyəti:</Col>
                <Col span={12} style={styles.desc}><b>Subay</b></Col>
            </Row>
            <Row>
                <Col span={12} style={styles.heading}>Ş.V. seriya nömrəsi:</Col>
                <Col span={12} style={styles.desc}><b>90309991</b></Col>
            </Row>
            <Row>
                <Col span={12} style={styles.heading}>Ş.V. FİN kodu:</Col>
                <Col span={12} style={styles.desc}><b>HE11OMR</b></Col>
            </Row>
                </Col>
            </Row>
        </Row>
    )
};

const styles = {
    color: {
        primary:    {color: 'rgba(0, 0, 0, 0.65)'},
        secondary:  {color: 'rgba(0, 0, 0, 0.45)'}
    },
    heading: {
        textAlign: 'left',
        paddingRight: 10,
    },
    desc: {
        textAlign: 'left',
    }
};

export default PersonalInfo