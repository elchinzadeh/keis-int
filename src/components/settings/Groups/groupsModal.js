import React from 'react';
import { Modal, Form, Input, InputNumber, Checkbox, Select, DatePicker, message } from 'antd';
import HelperApi from '../../../api/HelperApi'

const FormItem = Form.Item;
const RangePicker = DatePicker.RangePicker;
const Option = Select.Option;

class GroupsModalForm extends React.Component{
    constructor() {
        super();

        this.state = {
            selectCenter: true,
            centers: null
        }
    }

    componentDidMount(){
        this.getCenters()
    }

    componentDidUpdate() {
        if (!this.props.visible) {
            this.props.form.resetFields();
        }
    }

    getCenters = () => {
        HelperApi.getListCenters().then(response => {
            if (response.data.error === null) {
                this.setState({
                    centers: response.data.data.entities
                })
            }else{
                message.error(response.data.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    onChange = (e) => {
        this.setState({
            selectCenter: !e.target.checked
        });
        if (e.target.checked){
            this.props.form.resetFields('centerId')
        }
    };

    handleSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.modalSubmit(values)
            }
        })
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 8 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 16 },
            },
        };
        const dateFormat = 'DD.MM.YYYY';

        return(
            <Modal
                title="Qrupun əlavə olunması"
                visible={this.props.visible}
                onOk={this.handleSubmit}
                onCancel={this.props.onCancel}
                okText="Təsdiqlə"
                cancelText="Ləğv et"
                style={{ top: 20 }}
            >
                <Form>
                    <FormItem
                        {...formItemLayout}
                        label="Mərkəz"
                    >
                        <Checkbox onChange={e => this.onChange(e)}>
                            Bütün mərkəzlər
                        </Checkbox> <br/>

                        {getFieldDecorator('centerId', {
                            rules: [{ required: this.state.selectCenter, message: 'Mərkəzi seçin' }],
                        })(
                            <Select
                                disabled={!this.state.selectCenter}
                                showSearch
                                optionFilterProp="children"
                                placeholder="Mərkəz"
                            >
                                {this.state.centers ? this.state.centers.map(item =>
                                    <Option key={item.id} value={item.id}>{item.name}</Option>
                                ) : ''}

                            </Select>
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Qrup nömrəsi"
                    >
                        {getFieldDecorator('groupNo', {
                            rules: [{ required: true, message: 'Qrupun nömrəsini daxil edin' }],
                        })(
                            <InputNumber style={{width: '100%'}} placeholder="Qrup nömrəsi"/>
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Prefiks"
                    >
                        {getFieldDecorator('groupSuffix', {
                            rules: [{ required: true, message: 'Qrupun prefiksini daxil edin' }],
                        })(
                            <Input placeholder="Prefiks"/>
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Mövzu"
                    >
                        {getFieldDecorator('subject', {
                            rules: [{ required: true, message: 'Mövzunu daxil edin' }],
                        })(
                            <Input placeholder="Mövzu"/>
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Başlama - Bitmə"
                    >
                        {getFieldDecorator('startEndDate', {
                            rules: [{ required: true, message: 'Başlamavə bitmə tarixini daxil edin' }],
                        })(
                            <RangePicker format={dateFormat} />
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="İlk qiymətləndirmə"
                    >
                        {getFieldDecorator('firstAssessmentDate', {
                            rules: [{ required: true, message: 'İlk qiymətləndirmə tarixini daxil edin' }],
                        })(
                            <DatePicker style={{width: '100%'}} format={dateFormat} />
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="2-ci qiymətləndirmə"
                    >
                        {getFieldDecorator('secondAssessmentDate', {
                            rules: [{ required: true, message: '2-ci qiymətləndirmə tarixini daxil edin' }],
                        })(
                            <DatePicker style={{width: '100%'}} format={dateFormat} />
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Təlim tarixi"
                    >
                        {getFieldDecorator('trainingDate', {
                            rules: [{ required: true, message: 'Təlim tarixini daxil edin' }],
                        })(
                            <DatePicker style={{width: '100%'}} format={dateFormat} />
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="İmtahan tarixi"
                    >
                        {getFieldDecorator('examDate', {
                            rules: [{ required: true, message: 'İmtahan tarixini daxil edin' }],
                        })(
                            <DatePicker style={{width: '100%'}} format={dateFormat} />
                        )}
                    </FormItem>
                </Form>
            </Modal>
        )
    }
}

const GroupsModal = Form.create()(GroupsModalForm);

export default GroupsModal