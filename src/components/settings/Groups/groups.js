import React from 'react';
import { Col, Card, Table, Button, Popconfirm, Input, message } from 'antd';
import moment from 'moment';
import GroupsApi from '../../../api/m1/settings/GroupsApi';
import GroupsModal from './groupsModal';

export default class Groups extends React.Component {
    constructor() {
        super();

        this.state = {
            data: null,
            max: '',
            offset: 0,
            search: '',
            modalOpen: false,
        };

        this.columns = [
            {
                title: 'Adı',
                dataIndex: 'name',
                key: 'name',
            }, {
                title: 'Qrup nömrəsi',
                dataIndex: 'groupNo',
                key: 'groupNo',
            }, {
                title: 'Prefiks',
                dataIndex: 'groupSuffix',
                key: 'groupSuffix',
            }, {
                title: 'Mövzu',
                dataIndex: 'subject',
                key: 'subject',
            }, {
                title: '',
                dataIndex: 'id',
                key: 'id',
                render: (id) => (
                    <span>
                        <Popconfirm title="Məlumatı silməyə əminsinizmi?"
                                    onConfirm={() => this.deleteGroup(id)}
                                    okText="Bəli"
                                    cancelText="Xeyir"
                        >
                            <a>Sil</a>
                        </Popconfirm>
                    </span>
                ),
            }];

    }

    componentDidMount() {
        this.getGroups()
    }

    getGroups = () => {
        const config = {
            params: {
                max: this.state.max,
                offset: this.state.offset,
                search: this.state.search,
            }
        };

        GroupsApi.getAll(config).then(response => {
            if (response.data.error === null) {
                this.setState({
                    data: response.data.data.entities
                });
            }else{
                message.error(response.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    handleInsert = () => {
        this.toggleModal()
    };

    handleSearch = (value) => {
        console.log(value);

        const config = {
            params: {
                search: value,
            }
        };

        GroupsApi.getAll(config).then(response => {
            if (response.data.error === null) {
                this.setState({
                    data: response.data.data.entities
                });
            }else{
                message.error(response.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    toggleModal = () => {
        this.setState({
            modalOpen: !this.state.modalOpen
        });
    };

    insertGroup = (values) => {
        const body = {
            "name": values.name,
            "groupNo": values.groupNo,
            "groupSuffix": values.groupSuffix,
            "subject": values.subject,
            "startDate": moment(values.startEndDate[0]).valueOf(),
            "endDate": moment(values.startEndDate[1]).valueOf(),
            "firstAssesmentDate": moment(values.firstAssessmentDate).valueOf(),
            "secondAssesmentDate": moment(values.secondAssessmentDate).valueOf(),
            "trainingDate": moment(values.trainingDate).valueOf(),
            "examDate": moment(values.examDate).valueOf(),
            "centerId": {
                "id": values.centerId ? values.centerId : null
            }
        };

        console.log(body);

        GroupsApi.insert(body).then(response => {
            if (response.data.error === null) {
                message.success('Məlumat bazaya əlavə olundu');
                this.toggleModal();
                this.getGroups()
            }else{
                message.error(response.data.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    deleteGroup = (id) => {
        GroupsApi.delete(id).then(response => {
            if (response.data.error === null) {
                message.success('Məlumat bazadan silindi');
                this.getGroups()
            }else{
                message.error(response.data.error.message)
            }

        })
    };

    render() {
        return(
            <Card bordered={false}>
                <Input
                    placeholder="Axtarış"
                    onChange={value => this.handleSearch(value.target.value)}
                    style={{width: '35%', marginBottom: 24}}
                />
                <Button type="primary"
                        icon="plus"
                        style={{float: 'right'}}
                        onClick={() => this.handleInsert()}
                >
                    Əlavə et
                </Button>
                <Table
                    columns={this.columns}
                    dataSource={this.state.data}
                    rowKey={item => item.id}
                    expandedRowRender={record =>
                        <React.Fragment>
                            <Col span={12}>
                                <Col span={12}><p><b>Başlama tarixi: </b></p></Col>
                                <Col span={12}><p>{record.startDate ? moment(record.startDate).format('D MMMM YYYY') : ''}</p></Col>
                            </Col>
                            <Col span={12}>
                                <Col span={12}><p><b>Bitmə tarixi: </b></p></Col>
                                <Col span={12}><p>{record.endDate ? moment(record.endDate).format('D MMMM YYYY') : ''}</p></Col>
                            </Col>
                            <Col span={12}>
                                <Col span={12}><p><b>İlk qiymətləndirmə: </b></p></Col>
                                <Col span={12}><p>{record.firstAssessment ? moment(record.firstAssessment).format('D MMMM YYYY') : ''}</p></Col>
                            </Col>
                            <Col span={12}>
                                <Col span={12}><p><b>2-ci qiymətləndirmə: </b></p></Col>
                                <Col span={12}><p>{record.secondAssessment ? moment(record.secondAssessment).format('D MMMM YYYY') : ''}</p></Col>
                            </Col>
                            <Col span={12}>
                                <Col span={12}><p><b>Təlim tarixi: </b></p></Col>
                                <Col span={12}><p>{record.trainingDate ? moment(record.trainingDate).format('D MMMM YYYY') : ''}</p></Col>
                            </Col>
                            <Col span={12}>
                                <Col span={12}><p><b>İmtahan tarixi: </b></p></Col>
                                <Col span={12}><p>{record.examDate ? moment(record.examDate).format('D MMMM YYYY') : ''}</p></Col>
                            </Col>
                            <Col span={24}>
                                <Col span={6}><p><b>Mərkəz: </b></p></Col>
                                <Col span={18}><p>{record.centerId ? record.centerId.name : 'Bütün mərkəzlər'}</p></Col>
                            </Col>
                        </React.Fragment>
                    }
                />
                {
                    this.state.modalOpen ? <GroupsModal
                        visible={this.state.modalOpen}
                        selectedEntity={this.state.selectedEntity}
                        modalSubmit={this.insertGroup}
                        onCancel={this.toggleModal}
                    /> : null
                }
            </Card>
        )
    }
}