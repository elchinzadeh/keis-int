import React from 'react'
import { Row, Col, Button, Modal, Badge, Icon, message } from 'antd'
import TestQuestionsApi from '../../../api/m1/settings/TestQuestionsApi'


export default class TestQuestionShowModal extends React.Component {
    constructor() {
        super();

        this.state = {
            testQuestion :null,
            testQuestionVariants :null
        }
    }

    componentDidMount(){
        this.getTestQuestion()
    }

    getTestQuestion = () => {
        TestQuestionsApi.getById(this.props.selectedEntity).then(response => {
            if(response.data.error === null) {
                this.setState({
                    testQuestion: response.data.data.testQuestion,
                    testQuestionVariants: response.data.data.testQuestionVariants
                })
            }else{
                message.error(response.data.error.message)
            }
        })
    };

    deactivate = () => {
        TestQuestionsApi.deactivate(this.state.testQuestion.id).then(response => {
            if(response.data.error === null) {
                message.success('Məlumat yeniləndi');
                this.getTestQuestion();
                this.props.getTestQuestions()
            }else{
                message.error(response.data.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    activate = () => {
        TestQuestionsApi.activate(this.state.testQuestion.id).then(response => {
            if(response.data.error === null) {
                message.success('Məlumat yeniləndi');
                this.getTestQuestion();
                this.props.getTestQuestions()
            }else{
                message.error(response.data.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    render() {
        const questionType = {
                1: 'Yalnız yeni namizədlər',
                2: 'Yalnız məzunlar',
                3: 'Hərkəs',
            },
            difficultyLevel = {
                1: 'Əla',
                2: 'Yaxşı',
                3: 'orta',
                4: 'Zəif',
            };

        return(
            <Modal
                visible={this.props.visible}
                onCancel={this.props.onCancel}
                footer={false}
                className="TestQuestionsShowModal"
            >
                <Row>
                    {this.state.testQuestion ?
                        <React.Fragment>
                            <h2>{this.state.testQuestion.question}</h2>
                            <img src="https://cdn.lynda.com/course/477451/477451-636198978267245657-16x9.jpg" alt="" width="100%"/>
                        </React.Fragment>
                        : ''
                    }
                </Row>
                <br/>
                <Row>
                    {this.state.testQuestionVariants ?
                        this.state.testQuestionVariants.map(item =>
                            item.isTrue ?
                                <h4><Badge status="success"/> {item.answer}</h4> :
                                <h4><Badge status="default"/> {item.answer}</h4>
                        ) : ''
                    }
                </Row>
                <br/>
                <Row>
                    {this.state.testQuestion ?
                        <React.Fragment>
                            <Row>
                                <Col span={8}>
                                    <p><b>Kateqoriya: </b></p>
                                </Col>
                                <Col span={16}>
                                    <p>{this.state.testQuestion.questionCategoryId.name}</p>
                                </Col>
                            </Row>
                            <Row>
                                <Col span={8}>
                                    <p><b>Sual növü: </b></p>
                                </Col>
                                <Col span={16}>
                                    <p>{questionType[this.state.testQuestion.questionType]}</p>
                                </Col>
                            </Row>
                            <Row>
                                <Col span={8}>
                                    <p><b>Çətinlik dərəcəsi: </b></p>
                                </Col>
                                <Col span={16}>
                                    <p>{difficultyLevel[this.state.testQuestion.difficultyLevel]}</p>
                                </Col>
                            </Row>
                            <Row>
                                <Col span={8}>
                                    <p><b>Status: </b></p>
                                </Col>
                                <Col span={16}>
                                    <p>
                                        {this.state.testQuestion.isActive ? <p>Aktiv</p> : <p>Deaktiv</p>}
                                    </p>
                                </Col>
                            </Row>
                        </React.Fragment>
                        : ''
                    }
                </Row>
                <br/>
                <Row>
                    {this.state.testQuestion ?
                        this.state.testQuestion.isActive ?
                            <Button onClick={() => this.deactivate()} type="danger">Deaktiv et</Button> :
                            <Button onClick={() => this.activate()} type="primary">Aktiv et</Button> : ''
                    }
                </Row>
            </Modal>
        )
    }
}