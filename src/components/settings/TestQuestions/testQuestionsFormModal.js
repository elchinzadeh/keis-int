import React from 'react'
import { Modal, Form, Input, Select, Checkbox, Button, Radio, Icon, message } from 'antd'
import HelperApi from "../../../api/HelperApi";

const FormItem = Form.Item;
const Option = Select.Option;
let uuid = 0;


class TestQuestionFrom extends React.Component {
    constructor() {
        super();

        this.state = {
            categories: null
        }
    }

    componentDidMount(){
        this.getCategories()
    }

    componentDidUpdate() {
        if (!this.props.visible) {
            this.props.form.resetFields();
        }
    }

    getCategories = () => {
        HelperApi.getListTestQuestionCategory().then(response => {
            if (response.data.error === null) {
                this.setState({
                    categories: response.data.data.entities
                })
            }else{
                message.error(response.data.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
            }
            // this.props.modalSubmit(values)
            console.log('>>>>>', values);
        })
    };

    remove = (k) => {
        const { form } = this.props;
        // can use data-binding to get
        const keys = form.getFieldValue('keys');
        // We need at least one passenger
        if (keys.length === 1) {
            return;
        }

        // can use data-binding to set
        form.setFieldsValue({
            keys: keys.filter(key => key !== k),
        });
    };

    add = () => {
        const { form } = this.props;
        // can use data-binding to get
        const keys = form.getFieldValue('keys');
        const nextKeys = keys.concat(uuid);
        uuid++;
        // can use data-binding to set
        // important! notify form to detect changes
        form.setFieldsValue({
            keys: nextKeys,
        });
    };


    render() {
        const { getFieldDecorator, getFieldValue } = this.props.form;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 18 },
            },
        };
        const formItemLayoutWithOutLabel = {
            wrapperCol: {
                xs: { span: 24, offset: 0 },
                sm: { span: 18, offset: 6 },
            },
        };

        getFieldDecorator('keys', { initialValue: [] });
        const keys = getFieldValue('keys');

        const formItems = keys.map((k, index) => {
            return (
                <FormItem
                    {...(index === 0 ? formItemLayout : formItemLayoutWithOutLabel)}
                    label={index === 0 ? 'Sual variantı' : ''}
                    required={false}
                    key={k}
                >
                    {getFieldDecorator(`names[${k}]`, {
                        validateTrigger: ['onChange', 'onBlur'],
                        rules: [{
                            required: true,
                            whitespace: false,
                            message: "Zəhmət olmasa variantı əlavə edin və ya silin",
                        }],
                    })(
                        <Input placeholder="Sual variantı"
                               style={{ width: '90%', marginRight: 8 }}
                               addonBefore={<Radio>1</Radio>}
                        />
                    )}
                    {keys.length > 1 ? (
                        <Icon
                            className="dynamic-delete-button"
                            type="minus-circle-o"
                            disabled={keys.length === 1}
                            onClick={() => this.remove(k)}
                        />
                    ) : null}
                </FormItem>
            );
        });

        return(
            <Modal
                title="Test sualının əlavə olunması"
                visible={this.props.visible}
                onOk={this.handleSubmit}
                onCancel={this.props.onCancel}
                okText="Təsdiqlə"
                cancelText="Ləğv et"
                style={{ top: 20 }}
                width={640}
            >
                <Form>
                    <FormItem
                        {...formItemLayout}
                        label="Sual"
                    >
                        {getFieldDecorator('question', {
                            rules: [{ required: true, message: 'Sualı daxil edin' }],
                        })(
                            <Input placeholder="Sual"/>
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Kateqoriya"
                    >
                        {getFieldDecorator('questionCategoryId', {
                            rules: [{ required: true, message: 'Kateqoriyanı seçin' }],
                        })(
                            <Select
                                showSearch
                                optionFilterProp="children"
                                placeholder="Kateqoriya"
                            >
                                {this.state.categories ? this.state.categories.map(item =>
                                    <Option key={item.id} value={item.id}>{item.name}</Option>
                                ) : ''}

                            </Select>
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Sual növü"
                    >
                        {getFieldDecorator('questionType', {
                            rules: [{ required: true, message: 'Sual növünü seçin' }],
                        })(
                            <Select placeholder="Sual növü">
                                <Option key={1} value="1">Yalnız yeni namizədlər</Option>
                                <Option key={2} value="2">Yalnız məzunlar</Option>
                                <Option key={3} value="3">Hərkəs</Option>
                            </Select>
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Çətinlik dərəcəsi"
                    >
                        {getFieldDecorator('difficultyLevel', {
                            rules: [{ required: true, message: 'Çətinlik dərəcəsini seçin' }],
                        })(
                            <Select
                                placeholder="Çətinlik dərəcəsi"
                            >
                                <Option key={1} value="1">Əla</Option>
                                <Option key={2} value="2">Yaxşı</Option>
                                <Option key={3} value="3">Orta</Option>
                                <Option key={4} value="4">Zəif</Option>
                            </Select>
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Status"
                    >
                        {getFieldDecorator('isActive', {
                            rules: [{ required: false }],
                        })(
                            <Checkbox>Aktiv</Checkbox>,
                        )}
                    </FormItem>
                    {formItems}
                    <FormItem {...formItemLayoutWithOutLabel}>
                        <Button type="dashed" onClick={this.add} style={{ width: '90%' }}>
                            <Icon type="plus" /> Variant əlavə et
                        </Button>
                    </FormItem>
                </Form>
            </Modal>
        )
    }
}

const TestQuestionFromModal = Form.create()(TestQuestionFrom);

export default TestQuestionFromModal;