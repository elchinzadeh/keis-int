import React from 'react';
import { Col, Card, Table, Button, Badge, Divider, Popconfirm, Input, message } from 'antd';
import moment from "moment/moment";
import './testQuestions.css'
import TestQuestionsApi from "../../../api/m1/settings/TestQuestionsApi";
import TestQuestionsFormModal from './testQuestionsFormModal';
import TestQuestionsShowModal from './testQuestionsShowModal';


export default class TestQuestions extends React.Component {
    constructor() {
        super();

        this.state = {
            data: null,
            max: '',
            offset: 0,
            search: '',
            selectedEntity: null,
            formModalOpen: false,
            showModalOpen: false,
        };

        this.columns = [
            {
                title: 'Adı',
                dataIndex: 'question',
                key: 'question',
            }, {
                title: 'Status',
                dataIndex: 'isActive',
                render: isActive => isActive ? <Badge status="success" text="Aktiv" /> : <Badge status="error" text="Deaktiv" />
            }, {
                title: '',
                dataIndex: 'id',
                key: 'id',
                render: (id) => (
                    <span>
                        <a onClick={() => this.handleShow(id)}>Bax</a>
                        <Divider type="vertical"/>
                        <Popconfirm title="Məlumatı silməyə əminsinizmi?"
                                    onConfirm={() => this.deleteTestQuestion(id)}
                                    okText="Bəli"
                                    cancelText="Xeyir"
                        >
                            <a>Sil</a>
                        </Popconfirm>
                    </span>
                ),
            }];

    }

    componentDidMount() {
        this.getTestQuestions()
    }

    getTestQuestions = () => {
        const config = {
            params: {
                max: this.state.max,
                offset: this.state.offset,
                search: this.state.search,
            }
        };

        TestQuestionsApi.getAll(config).then(response => {
            if (response.data.error === null) {
                this.setState({
                    data: response.data.data.entities
                });
            }else{
                message.error(response.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    handleShow = (id) => {
        this.setState({
            selectedEntity: id
        });
        this.toggleShowModal()

    };

    handleInsert = () => {
        this.toggleFormModal()
    };

    handleSearch = (value) => {
        console.log(value);

        const config = {
            params: {
                search: value,
            }
        };

        TestQuestionsApi.getAll(config).then(response => {
            if (response.data.error === null) {
                this.setState({
                    data: response.data.data.entities
                });
            }else{
                message.error(response.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    toggleFormModal = () => {
        this.setState({
            formModalOpen: !this.state.formModalOpen
        });
    };

    toggleShowModal = () => {
        this.setState({
            showModalOpen: !this.state.showModalOpen
        });
    };

    insertTestQuestion = (values) => {
        const body = {
            "name": values.name,
            "groupNo": values.groupNo,
            "groupSuffix": values.groupSuffix,
            "subject": values.subject,
            "startDate": moment(values.startEndDate[0]).valueOf(),
            "endDate": moment(values.startEndDate[1]).valueOf(),
            "firstAssesmentDate": moment(values.firstAssessmentDate).valueOf(),
            "secondAssesmentDate": moment(values.secondAssessmentDate).valueOf(),
            "trainingDate": moment(values.trainingDate).valueOf(),
            "examDate": moment(values.examDate).valueOf(),
            "centerId": {
                "id": values.centerId ? values.centerId : null
            }
        };

        console.log(body);

        TestQuestionsApi.insert(body).then(response => {
            if (response.data.error === null) {
                message.success('Məlumat bazaya əlavə olundu');
                this.toggleModal();
                this.getTestQuestions()
            }else{
                message.error(response.data.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    deleteTestQuestion = (id) => {
        TestQuestionsApi.delete(id).then(response => {
            if (response.data.error === null) {
                message.success('Məlumat bazadan silindi');
                this.getTestQuestions()
            }else{
                message.error(response.data.error.message)
            }

        })
    };

    render() {
        return(
            <Card bordered={false}>
                <Input
                    placeholder="Axtarış"
                    onChange={value => this.handleSearch(value.target.value)}
                    style={{width: '35%', marginBottom: 24}}
                />
                <Button type="primary"
                        icon="plus"
                        style={{float: 'right'}}
                        onClick={() => this.handleInsert()}
                >
                    Əlavə et
                </Button>
                <Table
                    columns={this.columns}
                    dataSource={this.state.data}
                    rowKey={item => item.id}
                />
                {
                    this.state.formModalOpen ? <TestQuestionsFormModal
                        visible={this.state.formModalOpen}
                        selectedEntity={this.state.selectedEntity}
                        modalSubmit={this.insertTestQuestion}
                        onCancel={this.toggleFormModal}
                    /> : null
                }
                {
                    this.state.showModalOpen ? <TestQuestionsShowModal
                        visible={this.state.showModalOpen}
                        selectedEntity={this.state.selectedEntity}
                        onCancel={this.toggleShowModal}
                        getTestQuestions={this.getTestQuestions}
                    /> : null
                }
            </Card>
        )
    }
}