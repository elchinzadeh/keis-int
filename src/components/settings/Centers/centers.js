import React from 'react';
import { Card, Table, Button, Badge, Divider, Popconfirm, Input, message } from 'antd';
import CentersApi from "../../../api/m1/settings/CentersApi";
import CentersModal from './centersModal';

export default class Centers extends React.Component {
    constructor() {
        super();

        this.state = {
            data: null,
            max: '',
            offset: 0,
            search: '',
            modalOpen: false,
            selectedEntity: null
        };

        this.columns = [
            {
                title: 'Adı',
                dataIndex: 'name',
                key: 'name',
            }, {
                title: 'Status',
                dataIndex: 'isActive',
                render: isActive => isActive ? <Badge status="success" text="Aktiv" /> : <Badge status="error" text="Deaktiv" />
            }, {
                title: 'Mərkəz növü',
                dataIndex: 'centerTypeId.name',
                key: 'centerTypeId.id',
            }, {
                title: 'Şəhər / Rayon',
                dataIndex: 'districtCityId.name',
                key: 'districtCityId.id',
            }, {
                title: '',
                dataIndex: 'id',
                key: 'id',
                render: (id) => (
                    <span>
                        <a onClick={() => this.handleEdit(id)}>Düzəliş et</a>
                        <Divider type="vertical" />
                        <Popconfirm title="Məlumatı silməyə əminsinizmi?"
                                    onConfirm={() => this.deleteCenter(id)}
                                    okText="Bəli"
                                    cancelText="Xeyir"
                        >
                            <a>Sil</a>
                        </Popconfirm>
                    </span>
                ),
            }];

    }

    componentDidMount() {
        this.getCenters()
    }

    getCenters = () => {
        const config = {
            params: {
                max: this.state.max,
                offset: this.state.offset,
                search: this.state.search,
            }
        };

        CentersApi.getAll(config).then(response => {
            if (response.data.error === null) {
                this.setState({
                    data: response.data.data.entities
                });
            }else{
                message.error(response.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    handleEdit = (id) => {
        this.setState({
            selectedEntity: id
        });
        this.toggleModal()
    };

    handleInsert = () => {
        this.setState({
            selectedEntity: null
        });
        this.toggleModal()
    };

    handleSearch = (value) => {
        console.log(value);

        const config = {
            params: {
                search: value,
            }
        };

        CentersApi.getAll(config).then(response => {
            if (response.data.error === null) {
                this.setState({
                    data: response.data.data.entities
                });
            }else{
                message.error(response.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    toggleModal = () => {
        this.setState({
            modalOpen: !this.state.modalOpen
        });
    };

    modalSubmit = (values) => {
        if (this.state.selectedEntity === null) {
            this.insertCenter(values)
        }else{
            this.updateCenter(values)
        }
    };

    insertCenter = (values) => {
        const body = {
            "name": values.name,
            "isActive": values.isActive,
            "parentId": {
                "id": values.parentId
            },
            "centerTypeId": {
                "id": values.centerTypeId,
            },
            "districtCityId": {
                "id" : values.districtCityId
            }
        };

        CentersApi.insert(body).then(response => {
            if (response.data.error === null) {
                message.success('Məlumat bazaya əlavə olundu');
                this.toggleModal();
                this.getCenters()
            }else{
                message.error(response.data.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    updateCenter = (values) => {
        const body = {
            "id": this.state.selectedEntity,
            "name": values.name,
            "isActive": values.isActive,
            "parentId": {
                "id": values.parentId
            },
            "centerTypeId": {
                "id": values.centerTypeId,
            },
            "districtCityId": {
                "id" : values.districtCityId
            }
        };

        CentersApi.update(body).then(response => {
            if (response.data.error === null) {
                message.success('Məlumat yeniləndi');
                this.toggleModal();
                this.getCenters()
            }else{
                message.error(response.data.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    deleteCenter = (id) => {
        CentersApi.delete(id).then(response => {
            if (response.data.error === null) {
                message.success('Məlumat bazadan silindi');
                this.getCenters()
            }else{
                message.error(response.data.error.message)
            }

        })
    };

    render() {
        return(
            <Card bordered={false}>
                <Input
                    placeholder="Axtarış"
                    onChange={value => this.handleSearch(value.target.value)}
                    style={{width: '35%', marginBottom: 24}}
                />
                <Button type="primary"
                        icon="plus"
                        style={{float: 'right'}}
                        onClick={() => this.handleInsert()}
                >
                    Əlavə et
                </Button>
                <Table
                    columns={this.columns}
                    dataSource={this.state.data}
                    rowKey={item => item.id}
                />
                {
                    this.state.modalOpen ? <CentersModal
                            visible={this.state.modalOpen}
                            selectedEntity={this.state.selectedEntity}
                            modalSubmit={this.modalSubmit}
                            onCancel={this.toggleModal}
                        /> : null
                }
            </Card>
        )
    }
}