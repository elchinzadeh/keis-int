import React from 'react';
import { Modal, Form, Input, Checkbox, Select, message } from 'antd';
import CentersApi from '../../../api/m1/settings/CentersApi';
import HelperApi from '../../../api/HelperApi';

const FormItem = Form.Item;
const Option = Select.Option;

class CentersModalForm extends React.Component{
    constructor() {
        super();

        this.state = {
            centers: null,
            centerTypes: null,
            districtCities: null,
            selectedEntity: null
        }
    }

    componentDidMount() {
        if (this.props.selectedEntity) {
            this.getCenterById(this.props.selectedEntity)
        }
        this.getListCenters();
        this.getListCenterTypes();
        this.getListDistrictCity();
    }

    componentDidUpdate() {
        if (!this.props.visible) {
            this.props.form.resetFields();
        }
    }

    getCenterById = (id) => {
        CentersApi.getById(id).then(response => {
            if (response.data.error === null) {
                this.setState({
                    selectedEntity: response.data.data
                });
            }else{
                message.error(response.data.error.message)
            }
        });
    };

    getListCenters= () => {
        HelperApi.getListCenters().then(response => {
            if (response.data.error === null){
                this.setState({
                    centers: response.data.data.entities
                })
            }else{
                message.error(response.data.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    getListCenterTypes = () => {
        HelperApi.getListCenterType().then(response => {
            if (response.data.error === null){
                this.setState({
                    centerTypes: response.data.data.entities
                })
            }else{
                message.error(response.data.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    getListDistrictCity = () => {
        HelperApi.getListDistrictCity().then(response => {
            if (response.data.error === null){
                this.setState({
                    districtCities: response.data.data.entities
                })
            }else{
                message.error(response.data.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.modalSubmit(values)
            }
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 18 },
            },
        };

        return(
            <Modal
                title={ this.props.selectedEntity ? 'Mərkəzin redaktə olunması' : 'Mərkəzin əlavə olunması'}
                visible={this.props.visible}
                onOk={this.handleSubmit}
                onCancel={this.props.onCancel}
                okText="Təsdiqlə"
                cancelText="Ləğv et"
            >
                <Form>
                    <FormItem
                        {...formItemLayout}
                        label="Adı"
                    >
                        {getFieldDecorator('name', {
                            rules: [{ required: true, message: 'Mərkəzin adını daxil edin' }],
                            initialValue: this.state.selectedEntity ? this.state.selectedEntity.name : ''
                        })(
                            <Input placeholder="Adı:"/>
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Üst mərkəz"
                    >
                        {getFieldDecorator('parentId', {
                            rules: [{ required: false }],
                            initialValue: this.state.selectedEntity ? this.state.selectedEntity.parentId ? this.state.selectedEntity.parentId.id : null : undefined
                        })(
                            <Select
                                showSearch
                                placeholder="Üst mərkəz"
                                optionFilterProp="children"
                            >
                                <Option value={null} key={null}>Yoxdur</Option>
                                {
                                    this.state.centers ? this.state.centers.map(item =>
                                        <Option value={item.id} key={item.id}>{item.name}</Option>
                                    ) : ''
                                }
                            </Select>
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Mərkəz növü"
                    >
                        {getFieldDecorator('centerTypeId', {
                            rules: [{ required: true, message: 'Mərkəz növünü seçin' }],
                            initialValue: this.state.selectedEntity ? this.state.selectedEntity.centerTypeId.id : undefined
                        })(
                            <Select
                                showSearch
                                placeholder="Mərkəz növü"
                                optionFilterProp="children"
                            >
                                {
                                    this.state.centerTypes ? this.state.centerTypes.map(item =>
                                        <Option value={item.id} key={item.id}>{item.name}</Option>
                                    ) : ''
                                }
                            </Select>
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Şəhər / Rayon"
                    >
                        {getFieldDecorator('districtCityId', {
                            rules: [{ required: true, message: 'Şəhəri (Rayonu) seçin' }],
                            initialValue: this.state.selectedEntity ? this.state.selectedEntity.districtCityId.id : undefined
                        })(
                            <Select
                                showSearch
                                placeholder="Şəhər / Rayon"
                                optionFilterProp="children"
                            >
                                {
                                    this.state.districtCities ? this.state.districtCities.map(item =>
                                        <Option value={item.id} key={item.id}>{item.name}</Option>
                                    ) : ''
                                }
                            </Select>
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Status"
                    >
                        {getFieldDecorator('isActive', {
                            valuePropName: 'checked',
                            initialValue: true,
                        })(
                            <Checkbox>Aktiv</Checkbox>
                        )}
                    </FormItem>
                </Form>
            </Modal>
        )
    }
}

const CentersModal = Form.create()(CentersModalForm);

export default CentersModal