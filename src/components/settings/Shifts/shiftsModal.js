import React from 'react';
import { Modal, Form, Input, Checkbox, Select, TimePicker, message } from 'antd';
import moment from 'moment';
import ShiftsApi from '../../../api/m1/settings/ShiftsApi';

const FormItem = Form.Item;
const Option = Select.Option;

class ShiftsModalForm extends React.Component{
    constructor() {
        super();

        this.state = {
            selectedEntity: null
        }
    }

    componentDidMount() {
        if (this.props.selectedEntity) {
            this.getShiftById(this.props.selectedEntity)
        }
    }

    componentDidUpdate() {
        if (!this.props.visible) {
            this.props.form.resetFields();
        }
    }

    getShiftById = (id) => {
        ShiftsApi.getById(id).then(response => {
            if (response.data.error === null) {
                this.setState({
                    selectedEntity: response.data.data
                });
            }else{
                message.error(response.data.error.message)
            }
        });
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.modalSubmit(values)
            }
        });
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                xs: { span: 24 },
                sm: { span: 18 },
            },
        };
        const format = 'H:mm';

        return(
            <Modal
                title={ this.props.selectedEntity ? 'Növbənin redaktə olunması' : 'Növbənin əlavə olunması'}
                visible={this.props.visible}
                onOk={this.handleSubmit}
                onCancel={this.props.onCancel}
                okText="Təsdiqlə"
                cancelText="Ləğv et"
                className="ShiftsModal"
            >
                <Form>
                    <FormItem
                        {...formItemLayout}
                        label="Adı"
                    >
                        {getFieldDecorator('shiftName', {
                            rules: [{ required: true, message: 'Növbənin adını daxil edin' }],
                            initialValue: this.state.selectedEntity ? this.state.selectedEntity.shiftName : ''
                        })(
                            <Input placeholder="Adı:"/>
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Həftə içi"
                    >
                        {getFieldDecorator('weekdaysStartTime', {
                            rules: [{ required: false }],
                            initialValue: this.state.selectedEntity ? moment(moment(this.state.selectedEntity.weekdaysStartTime).format('H:mm'), 'H:mm') : null
                        })(
                            <TimePicker className="left-time-picker" format={format} />,
                        )}
                        {getFieldDecorator('weekdaysEndTime', {
                            rules: [{ required: false }],
                            initialValue: this.state.selectedEntity ? moment(moment(this.state.selectedEntity.weekdaysEndTime).format('H:mm'), 'H:mm') : null
                        })(
                            <TimePicker className="right-time-picker" format={format} />,
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Şənbə günü"
                    >
                        {getFieldDecorator('saturdayStartTime', {
                            rules: [{ required: false }],
                            initialValue: this.state.selectedEntity ? moment(moment(this.state.selectedEntity.saturdayStartTime).format('H:mm'), 'H:mm') : null
                        })(
                            <TimePicker className="left-time-picker" format={format} />,
                        )}
                        {getFieldDecorator('saturdayEndTime', {
                            rules: [{ required: false }],
                            initialValue: this.state.selectedEntity ? moment(moment(this.state.selectedEntity.saturdayEndTime).format('H:mm'), 'H:mm') : null
                        })(
                            <TimePicker className="right-time-picker" format={format} />,
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="Bazar günü"
                    >
                        {getFieldDecorator('sundayStartTime', {
                            rules: [{ required: false }],
                            initialValue: this.state.selectedEntity ? moment(moment(this.state.selectedEntity.sundayStartTime).format('H:mm'), 'H:mm') : null
                        })(
                            <TimePicker className="left-time-picker" format={format} />,
                        )}
                        {getFieldDecorator('sundayEndTime', {
                            rules: [{ required: false }],
                            initialValue: this.state.selectedEntity ? moment(moment(this.state.selectedEntity.sundayEndTime).format('H:mm'), 'H:mm') : null
                        })(
                            <TimePicker className="right-time-picker" format={format} />,
                        )}
                    </FormItem>

                    <FormItem
                        {...formItemLayout}
                        label="Status"
                    >
                        {getFieldDecorator('isActive', {
                            valuePropName: 'checked',
                            initialValue: true,
                        })(
                            <Checkbox>Aktiv</Checkbox>
                        )}
                    </FormItem>
                </Form>
            </Modal>
        )
    }
}

const ShiftsModal = Form.create()(ShiftsModalForm);

export default ShiftsModal