import React from 'react';
import { Card, Table, Button, Badge, Divider, Popconfirm, Input, message } from 'antd';
import moment from 'moment';
import ShiftsApi from "../../../api/m1/settings/ShiftsApi";
import ShiftsModal from './shiftsModal';
import './shifts.css';

export default class Shifts extends React.Component {
    constructor() {
        super();

        this.state = {
            data: null,
            max: '',
            offset: 0,
            search: '',
            modalOpen: false,
            selectedEntity: null
        };

        this.columns = [
            {
                title: 'Adı',
                dataIndex: 'shiftName',
                key: 'shiftNames',
            }, {
                title: 'Status',
                dataIndex: 'isActive',
                render: isActive => isActive ? <Badge status="success" text="Aktiv" /> : <Badge status="error" text="Deaktiv" />
            }, {
                title: 'Həftə içi',
                render: (item) => moment(item.weekdaysStartTime).format('H:mm') +' - '+ moment(item.weekdaysEndTime).format('H:mm')
            }, {
                title: 'Şənbə',
                render: (item) => moment(item.saturdayStartTime).format('H:mm') +' - '+ moment(item.saturdayEndTime).format('H:mm')
            }, {
                title: 'Bazar',
                render: (item) => moment(item.sundayStartTime).format('H:mm') +' - '+ moment(item.sundayEndTime).format('H:mm')
            }, {
                title: '',
                dataIndex: 'id',
                key: 'id',
                render: (id) => (
                    <span>
                        <a onClick={() => this.handleEdit(id)}>Düzəliş et</a>
                        <Divider type="vertical" />
                        <Popconfirm
                            title="Məlumatı silməyə əminsinizmi?"
                            onConfirm={() => this.deleteShift(id)}
                            okText="Bəli"
                            cancelText="Xeyir"
                        >
                            <a>Sil</a>
                        </Popconfirm>
                    </span>
                ),
            }];

    }

    componentDidMount() {
        this.getShifts()
    }

    getShifts = () => {
        const config = {
            params: {
                max: this.state.max,
                offset: this.state.offset,
                search: this.state.search,
            }
        };

        ShiftsApi.getAll(config).then(response => {
            if (response.data.error === null) {
                this.setState({
                    data: response.data.data.entities
                });
            }else{
                message.error(response.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    handleEdit = (id) => {
        this.setState({
            selectedEntity: id
        });
        this.toggleModal()
    };

    handleInsert = () => {
        this.setState({
            selectedEntity: null
        });
        this.toggleModal()
    };

    handleSearch = (value) => {
        const config = {
            params: {
                search: value,
            }
        };

        ShiftsApi.getAll(config).then(response => {
            if (response.data.error === null) {
                this.setState({
                    data: response.data.data.entities
                });
            }else{
                message.error(response.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    toggleModal = () => {
        this.setState({
            modalOpen: !this.state.modalOpen
        });
    };

    modalSubmit = (values) => {
        if (this.state.selectedEntity === null) {
            this.insertShift(values)
        }else{
            this.updateShift(values)
        }
    };

    insertShift = (values) => {
        const body = {
            "shiftName": values.shiftName,
            "weekdaysStartTime": moment(values.weekdaysStartTime).valueOf(),
            "weekdaysEndTime": moment(values.weekdaysEndTime).valueOf(),
            "saturdayStartTime": moment(values.saturdayStartTime).valueOf(),
            "saturdayEndTime": moment(values.saturdayEndTime).valueOf(),
            "sundayStartTime": moment(values.sundayStartTime).valueOf(),
            "sundayEndTime": moment(values.sundayEndTime).valueOf(),
            "isActive": values.isActive
        };

        console.log(body)

        ShiftsApi.insert(body).then(response => {
            if (response.data.error === null) {
                message.success('Məlumat bazaya əlavə olundu');
                this.toggleModal();
                this.getShifts()
            }else{
                message.error(response.data.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    updateShift = (values) => {
        const body = {
            "id": this.state.selectedEntity,
            "shiftName": values.shiftName,
            "weekdaysStartTime": moment(values.weekdaysStartTime).valueOf(),
            "weekdaysEndTime": moment(values.weekdaysEndTime).valueOf(),
            "saturdayStartTime": moment(values.saturdayStartTime).valueOf(),
            "saturdayEndTime": moment(values.saturdayEndTime).valueOf(),
            "sundayStartTime": moment(values.sundayStartTime).valueOf(),
            "sundayEndTime": moment(values.sundayEndTime).valueOf(),
            "isActive": values.isActive
        };
        console.log(moment(values.weekdaysStartTime).valueOf());
        console.log(body.weekdaysStartTime);

        ShiftsApi.update(body).then(response => {
            if (response.data.error === null) {
                message.success('Məlumat yeniləndi');
                this.toggleModal();
                this.getShifts()
            }else{
                message.error(response.data.error.message)
            }
        }).catch(error => {
            message.error(error.message)
        })
    };

    deleteShift = (id) => {
        ShiftsApi.delete(id).then(response => {
            if (response.data.error === null) {
                message.success('Məlumat bazadan silindi');
                this.getShifts()
            }else{
                message.error(response.data.error.message)
            }

        })
    };

    render() {
        return(
            <Card bordered={false}>
                <Input
                    placeholder="Axtarış"
                    onChange={value => this.handleSearch(value.target.value)}
                    style={{width: '35%', marginBottom: 24}}
                />
                <Button type="primary"
                        icon="plus"
                        style={{float: 'right'}}
                        onClick={() => this.handleInsert()}
                >
                    Əlavə et
                </Button>
                <Table
                    columns={this.columns}
                    dataSource={this.state.data}
                    rowKey={item => item.id}
                />
                {
                    this.state.modalOpen ? <ShiftsModal
                        visible={this.state.modalOpen}
                        selectedEntity={this.state.selectedEntity}
                        modalSubmit={this.modalSubmit}
                        onCancel={this.toggleModal}
                    /> : null
                }
            </Card>
        )
    }
}