import React from 'react';
import { Menu } from 'antd';
import { Link } from 'react-router-dom';
import './settingsSidebar.css'

const SettingsSidebar = (props) => {
    return (
        <Menu
            onClick={value => props.onMenuChange(value.key)}
            defaultSelectedKeys={['centers']}
            mode="inline"
            className="SettingsSidebar"
        >
            <Menu.Item key="centers" to="#">
                <Link to="#">
                    Mərkəzlər
                </Link>
            </Menu.Item>
            <Menu.Item key="shifts">
                <Link to="#">
                    Növbələr
                </Link>
            </Menu.Item>
            <Menu.Item key="groups">
                <Link to="#">
                    Quplar
                </Link>
            </Menu.Item>
            <Menu.Item key="testQuestions">
                <Link to="#">
                    Test bazası
                </Link>
            </Menu.Item>
        </Menu>
    )
};

export default SettingsSidebar