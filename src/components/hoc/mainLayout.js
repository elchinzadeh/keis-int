import React from 'react';
import Header from '../general/Header/header';


const MainLayout = (props) => {
    return(
        <React.Fragment>
            <Header/>
            {props.children}
        </React.Fragment>
    )
};

export default MainLayout