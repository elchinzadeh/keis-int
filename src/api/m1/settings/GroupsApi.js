import service from '../../service';

export default class GroupsApi {

    static getAll(config) {
        return service.get('m1/system/groups/getAll', config)
    }

    static getById(id) {
        return service.get('m1/system/groups/getById/' + id)
    }

    static insert(body) {
        return service.post('m1/system/groups/insert', body)
    }

    static delete(id) {
        return service.delete('m1/system/groups/delete/' + id)
    }

}