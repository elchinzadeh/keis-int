import service from '../../service';

export default class TestQuestionsApi {
    static getAll (config) {
        return service.get('m1/system/testQuestions/getAll', config)
    }

    static getById (id) {
        return service.get('m1/system/testQuestions/getById/' + id)
    }

    static insert (body) {
        return service.post('m1/system/testQuestions/insert', body)
    }

    static activate (id) {
        return service.put('m1/system/testQuestions/activate/' + id)
    }

    static deactivate (id) {
        return service.put('m1/system/testQuestions/deactivate/' + id)
    }

    static delete (id) {
        return service.delete('m1/system/testQuestions/delete/' + id)
    }
}