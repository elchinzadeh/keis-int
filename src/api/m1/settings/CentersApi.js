import service from '../../service';

export default class CentersApi {

    static getAll(config) {
        return service.get('m1/system/centers/getAll', config)
    }

    static getById(id) {
        return service.get('m1/system/centers/getById/' + id)
    }

    static insert(body) {
        return service.post('m1/system/centers/insert', body)
    }

    static update(body) {
        return service.put('m1/system/centers/update', body)
    }

    static delete(id) {
        return service.delete('m1/system/centers/delete/' + id)
    }

}
