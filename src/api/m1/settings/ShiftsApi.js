import service from '../../service';

export default class ShiftsApi {

    static getAll(config) {
        return service.get('m1/system/shifts/getAll', config)
    }

    static getById(id) {
        return service.get('m1/system/shifts/getById/' + id)
    }

    static insert(body) {
        return service.post('m1/system/shifts/insert', body)
    }

    static update(body) {
        return service.put('m1/system/shifts/update', body)
    }

    static delete(id) {
        return service.delete('m1/system/shifts/delete/' + id)
    }

}
