import service from '../../service'

export default class ProfileApi {
    static getByUserId(id) {
        return service.get('master1/volunteers/getByUserId/' + id)
    }
}