import axios from 'axios';

const service = axios.create({
    baseURL: 'http://192.168.20.245:8080/keis-int/',
    timeout: 10000,
    headers: {
        'Content-Type': 'application/json',
        'Accept-Language': 'az'
    }
});

export default service