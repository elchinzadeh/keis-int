import service from './service';

const preUrl = 'helper/getAllByTableName/';

export default class HelperApi {
    static getListTestQuestionCategory (config){
        return service.get(preUrl + 'ListTestQuestionCategory', config)
    }

    static getListCenters (config){
        return service.get(preUrl + 'Centers', config)
    }

    static getListCenterType (config){
        return service.get(preUrl + 'ListCenterType', config)
    }

    static getListDistrictCity (config){
        return service.get(preUrl + 'ListDistrictCity', config)
    }
}