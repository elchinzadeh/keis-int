import Loadable from "react-loadable";
import Loading from "../../../components/general/Loading/loading";

const AsyncDashboard = Loadable({
    loader: () =>
        import(/* webpackChunkName: "Dashboard" */ "../../components/Dashboard/dashboard"),
    loading: Loading
});

export default AsyncDashboard