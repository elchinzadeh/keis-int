import Loadable from "react-loadable";
import Loading from "../../../components/general/Loading/loading";

const AsyncSettings = Loadable({
    loader: () =>
        import(/* webpackChunkName: "Settings" */ "../../components/Settings/settings"),
    loading: Loading
});

export default AsyncSettings;