import Loadable from "react-loadable";
import Loading from "../../../components/general/Loading/loading";

const AsyncVolunteers = Loadable({
  loader: () =>
    import(/* webpackChunkName: "Volunteers" */ "../../components/Volunteers/volunteers"),
  loading: Loading
});

export default AsyncVolunteers;