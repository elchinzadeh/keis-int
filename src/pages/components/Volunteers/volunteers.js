import React from 'react';
import { Row, Col } from 'antd';
import './volunteers.css';
import MainLayout from '../../../components/hoc/mainLayout';
import VolunteerList from  '../../../components/volunteers/VolunteerList/volunteerList'
import VolunteerFilter from  '../../../components/volunteers/VolunteerFilter/volunteerFilter'
import VolunteerProfile from  '../../../components/volunteers/VolunteerProfile/volunteerProfile'

export default class Volunteers extends React.Component {
    constructor() {
        super();

        this.state = {
            volunteerId: null,
            profileOpen: false

        }
    }

    toggleProfile = () => {
        this.setState({
            profileOpen: !this.state.profileOpen
        })
    };

    render() {
        return (
            <MainLayout>
                <div className="Volunteers">
                    <Row gutter={16} style={{ marginLeft: -8, marginRight: -8 }}>
                        <Col span={6}>
                            <VolunteerFilter
                                visible={!this.state.profileOpen}
                            />
                        </Col>
                        <Col span={18}>
                            <VolunteerList
                                showProfile={this.toggleProfile}
                                visible={!this.state.profileOpen}
                            />
                        </Col>
                        {
                            this.state.profileOpen ?
                            <Col>
                                <VolunteerProfile
                                    visible={this.state.profileOpen}
                                    closeProfile={this.toggleProfile}
                                />
                            </Col>
                            : null
                        }
                    </Row>
                </div>
            </MainLayout>
        );
    }
}
