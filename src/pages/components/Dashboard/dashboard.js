import React from 'react';
import { Col, Card, List, Icon, Avatar } from 'antd';
import { ChartCard, MiniArea } from 'ant-design-pro/lib/Charts';
import NumberInfo from 'ant-design-pro/lib/NumberInfo';
import numeral from 'numeral';
import moment from 'moment';

import './dashboard.css'
import MainLayout from '../../../components/hoc/mainLayout';

const visitData = [];
const beginDay = new Date().getTime();
for (let i = 0; i < 20; i += 1) {
    visitData.push({
        x: moment(new Date(beginDay + (1000 * 60 * 60 * 24 * i))).format('YYYY-MM-DD'),
        y: Math.floor(Math.random() * 100) + 10,
    });
}

const listInterviews = [
    {
        href: 'http://ant.design',
        title: 'Elchin Zakizadeh',
        description: '',
        content: '',
    },
    {
        href: 'http://ant.design',
        title: 'Mushfig Bagirzade',
        description: 'Ant Design, a design language for background applications, is refined by Ant UED Team.',
        content: '',
    },
    {
        href: 'http://ant.design',
        title: 'Carl Sagan',
        description: 'Ant Design, a design language for background applications, is refined by Ant UED Team.',
        content: '',
    },
];

const listData = [];
for (let i = 0; i < 5; i++) {
    listData.push({
        href: 'http://ant.design',
        title: `ant design part ${i}`,
        description: 'Ant Design, a design language for background applications, is refined by Ant UED Team.',
        content: '',
    });
}

const IconText = ({ type, text }) => (
    <span>
    <Icon type={type} style={{ marginRight: 8 }} />
        {text}
  </span>
);

export default class Dashboard extends React.Component {



    render() {

        return (
            <MainLayout>
                <div className="Dashboard" style={{ paddingTop: 20 }}>
                    <Col md={6}>
                        <ChartCard
                            bordered={false}
                            title="Ümumi könüllü sayı"
                            total={numeral(56248).format('0,0')}
                            contentHeight={120}
                        >
                            <NumberInfo
                                subTitle={<span>Son ayda</span>}
                                total={numeral(1321).format('0,0')}
                                status="down"
                                theme="dark"
                                subTotal={17.1}
                            />
                            <MiniArea
                                line
                                height={45}
                                data={visitData}
                            />
                        </ChartCard>
                    </Col>
                    <Col md={6}>
                        <ChartCard
                            bordered={false}
                            title="Mərkəz üzrə könüllü sayı"
                            total={numeral(56248).format('0,0')}
                            contentHeight={120}
                        >
                            <NumberInfo
                                subTitle={<span>Son ayda</span>}
                                total={numeral(1321).format('0,0')}
                                status="up"
                                subTotal={17.1}
                            />
                            <MiniArea
                                line
                                height={45}
                                data={visitData}
                            />
                        </ChartCard>
                    </Col>
                    <Col md={6}>
                        <ChartCard
                            bordered={false}
                            title="Mərkəz üzrə könüllü sayı"
                            total={numeral(56248).format('0,0')}
                            contentHeight={120}
                        >
                            <NumberInfo
                                subTitle={<span>Son ayda</span>}
                                total={numeral(1321).format('0,0')}
                                status="up"
                                subTotal={17.1}
                            />
                            <MiniArea
                                line
                                height={45}
                                data={visitData}
                            />
                        </ChartCard>
                    </Col>
                    <Col md={6}>
                        <ChartCard
                            bordered={false}
                            title="Mərkəz üzrə könüllü sayı"
                            total={numeral(56248).format('0,0')}
                            contentHeight={120}
                        >
                            <NumberInfo
                                subTitle={<span>Son ayda</span>}
                                total={numeral(1321).format('0,0')}
                                status="up"
                                subTotal={17.1}
                            />
                            <MiniArea
                                line
                                height={45}
                                data={visitData}
                            />
                        </ChartCard>
                    </Col>
                    <Col md={8}>
                        <Card bordered={false} title="Tədbirlər">
                            <List
                                itemLayout="vertical"
                                size="large"
                                pagination={{
                                    onChange: (page) => {
                                        console.log(page);
                                    },
                                    pageSize: 3,
                                }}
                                dataSource={listData}
                                renderItem={item => (
                                    <List.Item
                                        key={item.title}
                                        actions={[<IconText type="star-o" text="156" />, <IconText type="like-o" text="156" />, <IconText type="message" text="2" />]}
                                    >
                                        <List.Item.Meta
                                            avatar={<Avatar src={item.avatar} />}
                                            title={<a href={item.href}>{item.title}</a>}
                                            description={item.description}
                                        />
                                        {item.content}
                                    </List.Item>
                                )}
                            />
                        </Card>
                    </Col>
                    <Col md={8}>
                        <Card bordered={false} title="Müsahibələr">
                            <List
                                itemLayout="vertical"
                                size="large"
                                pagination={{
                                    onChange: (page) => {
                                        console.log(page);
                                    },
                                    pageSize: 3,
                                }}
                                dataSource={listInterviews}
                                renderItem={item => (
                                    <List.Item
                                        key={item.title}
                                        actions={[<IconText type="star-o" text="156" />, <IconText type="like-o" text="156" />, <IconText type="message" text="2" />]}
                                    >
                                        <List.Item.Meta
                                            avatar={<Avatar src={item.avatar} />}
                                            title={<a href={item.href}>{item.title}</a>}
                                            description={item.description}
                                        />
                                        {item.content}
                                    </List.Item>
                                )}
                            />
                        </Card>
                    </Col>
                </div>
            </MainLayout>
        );
    }
}
