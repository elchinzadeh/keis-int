import React from 'react';
import { Col } from 'antd';
import MainLayout from '../../../components/hoc/mainLayout';
import SettingsSidebar from '../../../components/sidebars/Settings/settingsSidebar';
import './settings.css';
import Centers from '../../../components/settings/Centers/centers';
import Groups from "../../../components/settings/Groups/groups";
import Shifts from "../../../components/settings/Shifts/shifts";
import TestQuestions from "../../../components/settings/TestQuestions/testQuestions";

export default class Settings extends React.Component {

    constructor() {
        super();

        this.state = {
            current: 'testQuestions'
        }
    }

    onMenuChange = (value) => {
        this.setState({
            current: value
        })
    };

    render() {
        const component = {
            centers: <Centers/>,
            groups: <Groups/>,
            shifts: <Shifts/>,
            testQuestions: <TestQuestions/>,
        };

        return(
            <MainLayout>
                <Col span={4}>
                    <SettingsSidebar onMenuChange={this.onMenuChange}/>
                </Col>
                <Col span={20}>
                    <div className="Settings">
                        {component[this.state.current]}
                    </div>
                </Col>
            </MainLayout>
        )
    }
}