import React from 'react';
import { Layout, Icon } from 'antd';

export const NotFound = () => {
    return(
        <React.Fragment>
            <Layout>
                <Layout.Content align="center">
                    <h1><Icon type="file-unknown" /></h1>
                    <h3>Axtardığınız səhifə tapılmadı</h3>
                </Layout.Content>
            </Layout>
        </React.Fragment>
    )
};

