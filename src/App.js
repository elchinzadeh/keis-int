import React, { Component } from "react";
import { Layout } from "antd";
import Helmet from "react-helmet";

import moment from 'moment';
import 'moment/locale/az';
import "./App.css";
import Routes from './components/general/Routes/routes'

moment.locale('az');

class App extends Component {
    render() {
        return (
            <div className="App">
                <Helmet>
                    <title>KEIS Intranet</title>
                </Helmet>
                <Layout className="MainLayout">
                    <Layout.Content>
                        <Routes/>
                    </Layout.Content>
                </Layout>
            </div>
        );
    }
}

export default App;
